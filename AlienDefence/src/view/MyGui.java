package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MyGui extends JFrame {

	private JPanel contentPane;
	private JLabel label;
	private JButton btnGreen;
	private JButton btnBlue;
	private JButton btnYellow;
	private JButton btnStandard;
	private JButton btnChooseCol;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGui frame = new MyGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 616);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		label = new JLabel(getText(txtHierBitteText));
		label.setBounds(10, 11, 390, 14);
		contentPane.add(label);

		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRed.setBounds(10, 48, 107, 23);
		contentPane.add(btnRed);

		btnGreen = new JButton("Gruen");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnGreen.setBounds(150, 48, 118, 23);
		contentPane.add(btnGreen);

		btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlue.setBounds(296, 48, 107, 23);
		contentPane.add(btnBlue);

		btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnYellow.setBounds(10, 99, 107, 23);
		contentPane.add(btnYellow);

		btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandard.setBounds(150, 99, 118, 23);
		contentPane.add(btnStandard);

		btnChooseCol = new JButton("Farbe w\u00E4hlen");
		btnChooseCol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color choice = JColorChooser.showDialog(contentPane, "Select a Color", Color.black);
				contentPane.setBackground(choice);
			}
		});
		btnChooseCol.setBounds(296, 99, 107, 23);
		contentPane.add(btnChooseCol);

		JLabel lblTask1 = DefaultComponentFactory.getInstance().createTitle("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblTask1.setBounds(11, 36, 390, 14);
		contentPane.add(lblTask1);

		JLabel lblTask2 = DefaultComponentFactory.getInstance().createTitle("Aufgabe 2: Text formatieren");
		lblTask2.setBounds(11, 133, 392, 14);
		contentPane.add(lblTask2);

		JLabel lblTask3 = DefaultComponentFactory.getInstance().createTitle("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblTask3.setBounds(11, 274, 392, 14);
		contentPane.add(lblTask3);

		JLabel lblTask4 = DefaultComponentFactory.getInstance()
				.createTitle("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblTask4.setBounds(11, 352, 392, 14);
		contentPane.add(lblTask4);

		JLabel lblTask5 = DefaultComponentFactory.getInstance().createTitle("Aufgabe 5: Textausrichtung");
		lblTask5.setBounds(10, 422, 393, 14);
		contentPane.add(lblTask5);

		JLabel lblTask6 = DefaultComponentFactory.getInstance().createTitle("Aufgabe 6: Programm beenden");
		lblTask6.setBounds(10, 484, 393, 14);
		contentPane.add(lblTask6);

		JButton btnTypoArial = new JButton("Arial");
		btnTypoArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnTypoArial.setBounds(10, 158, 107, 23);
		contentPane.add(btnTypoArial);

		JButton btnTypoSans = new JButton("Comic Sans MS");
		btnTypoSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnTypoSans.setBounds(150, 158, 118, 23);
		contentPane.add(btnTypoSans);

		JButton btnTypoCourier = new JButton("Courier New");
		btnTypoCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnTypoCourier.setBounds(296, 158, 107, 23);
		contentPane.add(btnTypoCourier);

		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(13, 192, 390, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);

		JButton btnToLabel = new JButton("Ins Label schreiben");
		btnToLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setText(txtHierBitteText.getText());
			}
		});
		btnToLabel.setBounds(10, 223, 192, 23);
		contentPane.add(btnToLabel);

		JButton btnLabelDelete = new JButton("Text im Label l\u00F6schen");
		btnLabelDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setText("");
			}
		});
		btnLabelDelete.setBounds(224, 223, 179, 23);
		contentPane.add(btnLabelDelete);

		JButton btnTypoRed = new JButton("Rot");
		btnTypoRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setForeground(Color.RED);
			}
		});
		btnTypoRed.setBounds(10, 299, 89, 23);
		contentPane.add(btnTypoRed);

		JButton btnTypoBlue = new JButton("Blau");
		btnTypoBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setForeground(Color.BLUE);
			}
		});
		btnTypoBlue.setBounds(164, 299, 89, 23);
		contentPane.add(btnTypoBlue);

		JButton btnTypoBlack = new JButton("Schwarz");
		btnTypoBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setForeground(Color.BLACK);
			}
		});
		btnTypoBlack.setBounds(314, 299, 89, 23);
		contentPane.add(btnTypoBlack);

		JButton btnTypoSizePlus = new JButton("+");
		btnTypoSizePlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = label.getFont().getSize();
				String schriftart = label.getFont().getFamily();
				label.setFont(new Font(schriftart, Font.PLAIN, groesse + 1));
			}
		});
		btnTypoSizePlus.setBounds(10, 381, 192, 23);
		contentPane.add(btnTypoSizePlus);

		JButton btnTypoSizeMinus = new JButton("-");
		btnTypoSizeMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = label.getFont().getSize();
				String schriftart = label.getFont().getFamily();
				label.setFont(new Font(schriftart, Font.PLAIN, groesse - 1));
			}
		});
		btnTypoSizeMinus.setBounds(211, 381, 192, 23);
		contentPane.add(btnTypoSizeMinus);

		JButton btnleft = new JButton("linksb\u00FCndig");
		btnleft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnleft.setBounds(10, 447, 118, 23);
		contentPane.add(btnleft);

		JButton btnCentral = new JButton("zentriert");
		btnCentral.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnCentral.setBounds(164, 450, 89, 23);
		contentPane.add(btnCentral);

		JButton btnRight = new JButton("rechtsb\u00FCndig");
		btnRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				label.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRight.setBounds(285, 450, 118, 23);
		contentPane.add(btnRight);

		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 517, 393, 60);
		contentPane.add(btnExit);
	}

	private String getText(JTextField txtHierBitteText2) {
		// TODO Auto-generated method stub
		return null;
	}
}
