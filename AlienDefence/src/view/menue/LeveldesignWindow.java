package view.menue;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.LevelController;
import controller.TargetController;
import model.Level;
import model.User;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class LeveldesignWindow extends JFrame {

	private LevelController lvlControl;
	private JPanel contentPane;
	private LevelChooser cardChooseLevel;
	private LevelEditor cardLevelEditor;
	private CardLayout cards;
	private double screenWidth;
	private double screenHeight;

	/**
	 * Create the frame.
	 */
	public LeveldesignWindow(LevelController lvlControl, TargetController targetControl, User user, AlienDefenceController alienDefenceController) {
		this.lvlControl = lvlControl;

		setTitle("Leveldesigner");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.screenHeight= Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		this.screenWidth= Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.BLACK);
		setContentPane(contentPane);
		this.cards = new CardLayout();
		contentPane.setLayout(cards);

		this.cardChooseLevel = new LevelChooser(lvlControl, this, user, alienDefenceController);
		contentPane.add(cardChooseLevel, "levelChooser");

		this.cardLevelEditor = new LevelEditor(this, lvlControl, targetControl, Level.getDefaultLevel());
		contentPane.add(cardLevelEditor, "levelEditor");

		this.showLevelChooser();
		this.setVisible(true);
	}

	/**
	 * display leveleditor with a new level
	 */
	public void startLevelEditor() {
		this.cardLevelEditor.setLvl(this.lvlControl.createLevel());
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * disply leveleditor with a new level
	 * 
	 * @param level_id
	 */
	public void startLevelEditor(int level_id) {
		this.cardLevelEditor.setLvl(this.lvlControl.readLevel(level_id));
		this.cards.show(contentPane, "levelEditor");
	}

	/**
	 * display a jTable with all Levels
	 */
	public void showLevelChooser() {
		this.cards.show(contentPane, "levelChooser");
		this.cardChooseLevel.updateTableData();
	}

}
